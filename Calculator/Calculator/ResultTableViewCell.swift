//
//  ResultTableViewCell.swift
//  Calculator
//
//  Created by Frostov Egor on 26.02.17.
//  Copyright © 2017 Frostov Egor. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {

    @IBOutlet weak var resultLabel: UILabel!
    
    override func prepareForReuse() {
        resultLabel.text = ""
    }
}
