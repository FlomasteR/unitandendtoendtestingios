
import XCTest

class CalculatorUITests: XCTestCase {
    
    var app:XCUIApplication!
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
        app = XCUIApplication()
    }
    
    /**
     Тестирование нескольких операций
     
     
     */
    
    func testCalacAndLabelResul() {
        
        app.buttons["5"].tap()
        app.buttons["+"].tap()
        app.buttons["6"].tap()
        app.buttons["="].tap()
        app.buttons["-"].tap()
        app.buttons["2"].tap()
        app.buttons["="].tap()
        app.buttons["/"].tap()
        app.buttons["3"].tap()
        app.buttons["="].tap()
        app.buttons["*"].tap()
        app.buttons["4"].tap()
        app.buttons["="].tap()
        
        let label = app.staticTexts.element(matching: .any, identifier: "Result").label
        
        XCTAssertEqual("12", label)
    }
    
    /**
     Тестирование сохранения результатов и корректность их расположения в таблице
     
     
     */
    
    func testCalcAndSaveResult() {
        
        let calculatorCalculatorviewNavigationBar = app.navigationBars["Calculator.CalculatorView"]
        let addButton = calculatorCalculatorviewNavigationBar.children(matching: .button).element(boundBy: 0)
        let listButton = calculatorCalculatorviewNavigationBar.children(matching: .button).element(boundBy: 1)
        
        app.buttons["5"].tap()
        app.buttons["+"].tap()
        app.buttons["6"].tap()
        app.buttons["="].tap()
        
        addButton.tap()
        
        let resultLabel1 = getResultLabel()
        
        listButton.tap()
        
        XCTAssertEqual(getCellLabel(indexElement: 0), resultLabel1)
        
        app.navigationBars["Calculator.ResultsView"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
        app.buttons["C"].tap()
        app.buttons["6"].tap()
        app.buttons["/"].tap()
        app.buttons["2"].tap()
        app.buttons["="].tap()
        
        addButton.tap()
        
        let resultLabel2 = getResultLabel()
        
        listButton.tap()
        
        XCTAssertEqual(getCellLabel(indexElement: 0), resultLabel2)
        XCTAssertEqual(getCellLabel(indexElement: 1), resultLabel1)
    }
    
    func getCellLabel(indexElement: UInt) -> String{
        let cells = app.tables.cells
        let staticTextOfFirstCell = cells.element(boundBy: indexElement).staticTexts.element(boundBy: 0)
        return staticTextOfFirstCell.label
    }
    
    func getResultLabel() -> String {
        return app.staticTexts.element(matching: .any, identifier: "Result").label
    }
    
}
