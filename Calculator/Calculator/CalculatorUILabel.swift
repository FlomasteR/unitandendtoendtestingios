//
//  calculatorUILabel.swift
//  Calculator
//
//  Created by Frostov Egor on 26.02.17.
//  Copyright © 2017 Frostov Egor. All rights reserved.
//

import UIKit

protocol CalculatorUILabelDelegate {
    func textDidChange(string: String)
}

class CalculatorUILabel: UILabel {
    
    var delegate: CalculatorUILabelDelegate? = nil
    
    override var text: String? {
        didSet {
            if let delegate = self.delegate {
                delegate.textDidChange(string: self.text!)
            }
        }
    }
}
