
import UIKit

enum TypeOperation {
    case pluse
    case minus
    case multiply
    case divide
    case none
}

class CalculatorViewController: UIViewController, CalculatorUILabelDelegate {
    
    @IBOutlet weak var textLabel: CalculatorUILabel!
    
    var firstArgument: String? = nil
    var secondArgument: String? = nil
    
    var dotOnScreen: Bool = false
    
    var nowOperation: TypeOperation = .none
    var resultArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textLabel.delegate = self
    }
    
    //MARK: - CalculatorUILabelDelegate
    
    func textDidChange(string: String) {
        changeTextLabelFont(fontSize: textLabel.font.pointSize)
        if string.range(of: ".") != nil {
            dotOnScreen = true
        } else {
            dotOnScreen = false
        }
    }
    
    //MARK: - Controller Actions
    
    @IBAction func operationAction(_ sender: UIButton) {
        if let buttonType = sender.titleLabel?.text {
            switch buttonType {
            case "+":
                setFirstArgumentAndOperation(operation: .pluse)
                break
            case "-":
                setFirstArgumentAndOperation(operation: .minus)
                break
            case "*":
                setFirstArgumentAndOperation(operation: .multiply)
                break
            case "/":
                setFirstArgumentAndOperation(operation: .divide)
                break
            default:
                nowOperation = .none
            }
        }
    }
    
    @IBAction func equalsAction(_ sender: UIButton) {
        guard let tempFirstArgument = self.firstArgument, let firstArgument = Double(tempFirstArgument) else {
            return
        }
        guard let tempSecondArgument = self.textLabel.text, let secondArgument = Double(tempSecondArgument) else {
            return
        }
        guard nowOperation != .none else {
            return
        }
        
        textLabel.text = calcResult(firstArgument: Double(firstArgument),
                               secondArgument: Double(secondArgument),
                               nowOperation: self.nowOperation)
    }
    
    @IBAction func saveResult(_ sender: UIBarButtonItem) {
        if let textLabelText = textLabel.text {
            resultArray.insert(textLabelText, at: 0)
        }
    }
    
    @IBAction func showResultViewController(_ sender: UIBarButtonItem) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ResultsViewController") as!ResultsViewController
        vc.resultArray = self.resultArray
        print(self.resultArray)
        self.show(vc, sender: nil)
    }
    
    @IBAction func nuberAction(_ sender: UIButton) {
        if let number: String = sender.titleLabel?.text {
            if (textLabel.text == "0") {
                textLabel.text = number
                changeTextLabelFont(fontSize: textLabel.font.pointSize)
                return
            }
            textLabel.text = textLabel.text! + number
            changeTextLabelFont(fontSize: textLabel.font.pointSize)
        }
    }
    
    @IBAction func addDot(_ sender: UIButton) {
        guard !dotOnScreen else {
            return
        }
        if let dot: String = sender.titleLabel?.text {
            textLabel.text = textLabel.text! + dot
        }
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        deleteLastNumber()
    }
    
    @IBAction func deleteAllAction(_ sender: UIButton) {
        deleteAll()
    }
    
    //MARK: - Controller Functions
    
    func calcResult(firstArgument: Double, secondArgument: Double, nowOperation: TypeOperation) -> String {
        switch nowOperation {
        case .pluse:
            return deleteDotAndZero(value: firstArgument + secondArgument)
        case .minus:
            return deleteDotAndZero(value: firstArgument - secondArgument)
        case .multiply:
            if (firstArgument == 0 || secondArgument == 0) {
                return "0"
            }
            return deleteDotAndZero(value: firstArgument * secondArgument)
        case .divide:
            if (secondArgument == 0) { return "00" }
            if (firstArgument == 0) { return "0" }
            return deleteDotAndZero(value: firstArgument / secondArgument)
        default:
            deleteAll()
            return "0"
        }
    }
    
    func deleteDotAndZero(value: Double) -> String {
        if (value.truncatingRemainder(dividingBy: 1) == 0) {
            return String(String(value).characters.dropLast(2))
        } else {
            return String(value)
        }
    }
    
    func setFirstArgumentAndOperation(operation: TypeOperation) {
        firstArgument = textLabel.text
        nowOperation = operation
        setZero()
    }
    
    func setZero() {
        textLabel.text = "0"
        textLabel.font = UIFont(name: textLabel.font.fontName, size: 30.0)
    }
    
    func deleteAll() {
        firstArgument = "0"
        secondArgument = nil
        nowOperation = .none
        textLabel.text = "0"
        textLabel.font = UIFont(name: textLabel.font.fontName, size: 30.0)
    }
    
    func deleteLastNumber() {
        if (textLabel.text?.characters.count == 1) {
            textLabel.text = "0"
        } else {
            textLabel.text = textLabel.text?.substring(to: (textLabel.text?.index(before: (textLabel.text?.endIndex)!))!)
        }
    }
    
    func changeTextLabelFont(fontSize: CGFloat) {
        if (fontSize > 14) {
            let labelText = textLabel.text
            if (textLabel.frame.width <= (labelText?.widthOfString(usingFont: textLabel.font))!) {
                textLabel.font = UIFont(name: textLabel.font.fontName, size: fontSize - 1)
                if (textLabel.frame.width <= (labelText?.widthOfString(usingFont: textLabel.font))!) {
                    changeTextLabelFont(fontSize: textLabel.font.pointSize)
                }
            }
        }
    }
}
