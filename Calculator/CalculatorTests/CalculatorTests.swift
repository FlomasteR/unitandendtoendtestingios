//
//  CalculatorTests.swift
//  CalculatorTests
//
//  Created by Frostov Egor on 24.02.17.
//  Copyright © 2017 Frostov Egor. All rights reserved.
//

import XCTest
@testable import Calculator

class CalculatorUnitTests: XCTestCase {
    
    var vc: CalculatorViewController!
    
    override func setUp() {
        super.setUp()
        vc = CalculatorViewController()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /**
     Тестирование операции сложения
     
     
     */
    
    func testPlusPositivNumbers() {
        XCTAssertEqual("10", vc.calcResult(firstArgument: 5, secondArgument: 5, nowOperation: .pluse))
        XCTAssertEqual("6.66", vc.calcResult(firstArgument: 2.3, secondArgument: 4.36, nowOperation: .pluse))
    }
    
    func testPlusNegativeNumbers() {
        XCTAssertEqual("-10", vc.calcResult(firstArgument: -5, secondArgument: -5, nowOperation: .pluse))
        XCTAssertEqual("-6.66", vc.calcResult(firstArgument: -2.3, secondArgument: -4.36, nowOperation: .pluse))
    }
    
    func testPluseOnePositiveOneNegativeNumbers() {
        XCTAssertEqual("-1", vc.calcResult(firstArgument: 5, secondArgument: -6, nowOperation: .pluse))
        XCTAssertEqual("-2.06", vc.calcResult(firstArgument: 2.3, secondArgument: -4.36, nowOperation: .pluse))
    }
    
    func testPluseWithZero() {
        XCTAssertEqual("5", vc.calcResult(firstArgument: 5, secondArgument: 0, nowOperation: .pluse))
        XCTAssertEqual("0", vc.calcResult(firstArgument: 0, secondArgument: 0, nowOperation: .pluse))
        XCTAssertEqual("-2", vc.calcResult(firstArgument: 0, secondArgument: -2, nowOperation: .pluse))
    }
    
    /**
     Тестирование операции вычитания
     
     
     */
    
    func testMinusPositivNumbers() {
        XCTAssertEqual("5", vc.calcResult(firstArgument: 10, secondArgument: 5, nowOperation: .minus))
        XCTAssertEqual("2.18", vc.calcResult(firstArgument: 4.3, secondArgument: 2.12, nowOperation: .minus))
    }
    
    func testMinusNegativeNumbers() {
        XCTAssertEqual("-2", vc.calcResult(firstArgument: -7, secondArgument: -5, nowOperation: .minus))
        XCTAssertEqual("2.88", vc.calcResult(firstArgument: -4.7, secondArgument: -7.58, nowOperation: .minus))
    }
    
    func testMinusOnePositiveOneNegativeNumbers() {
        XCTAssertEqual("11", vc.calcResult(firstArgument: 5, secondArgument: -6, nowOperation: .minus))
        XCTAssertEqual("6.66", vc.calcResult(firstArgument: 2.3, secondArgument: -4.36, nowOperation: .minus))
    }
    
    func testMinusWithZero() {
        XCTAssertEqual("4", vc.calcResult(firstArgument: 4, secondArgument: 0, nowOperation: .minus))
        XCTAssertEqual("0", vc.calcResult(firstArgument: 0, secondArgument: 0, nowOperation: .minus))
        XCTAssertEqual("3", vc.calcResult(firstArgument: 0, secondArgument: -3, nowOperation: .minus))
    }
    
    /**
     Тестирование операции умножения
     
     
     */
    
    func testMultiplicationPositivNumbers() {
        XCTAssertEqual("12", vc.calcResult(firstArgument: 3, secondArgument: 4, nowOperation: .multiply))
        XCTAssertEqual("2.464", vc.calcResult(firstArgument: 2.2, secondArgument: 1.12, nowOperation: .multiply))
    }
    
    func testMultiplicationNegativeNumbers() {
        XCTAssertEqual("6", vc.calcResult(firstArgument: -2, secondArgument: -3, nowOperation: .multiply))
        XCTAssertEqual("35.626", vc.calcResult(firstArgument: -4.7, secondArgument: -7.58, nowOperation: .multiply))
    }
    
    func testMultiplicationOnePositiveOneNegativeNumbers() {
        XCTAssertEqual("-434", vc.calcResult(firstArgument: 7, secondArgument: -62, nowOperation: .multiply))
        XCTAssertEqual("-18.27", vc.calcResult(firstArgument: 2.1, secondArgument: -8.7, nowOperation: .multiply))
    }
    
    func testMultiplicationWithZero() {
        XCTAssertEqual("0", vc.calcResult(firstArgument: 4, secondArgument: 0, nowOperation: .multiply))
        XCTAssertEqual("0", vc.calcResult(firstArgument: 0, secondArgument: 0, nowOperation: .multiply))
        XCTAssertEqual("0", vc.calcResult(firstArgument: 0, secondArgument: -3, nowOperation: .multiply))
    }
    
    /**
     Тестирование операции деления
     
     
     */
    
    func testDivisionPositivNumbers() {
        XCTAssertEqual("3", vc.calcResult(firstArgument: 12, secondArgument: 4, nowOperation: .divide))
        print(vc.calcResult(firstArgument: 8.2, secondArgument: 2.1, nowOperation: .divide))
        XCTAssertEqual("2", vc.calcResult(firstArgument: 4.4, secondArgument: 2.2, nowOperation: .divide))
    }
    
    func testDivisionNegativeNumbers() {
        XCTAssertEqual("4", vc.calcResult(firstArgument: -8, secondArgument: -2, nowOperation: .divide))
        XCTAssertEqual("2", vc.calcResult(firstArgument: -4.4, secondArgument: -2.2, nowOperation: .divide))
    }
    
    func testDivisionOnePositiveOneNegativeNumbers() {
        XCTAssertEqual("-7", vc.calcResult(firstArgument: 49, secondArgument: -7, nowOperation: .divide))
        XCTAssertEqual("-2", vc.calcResult(firstArgument: 4.4, secondArgument: -2.2, nowOperation: .divide))
    }
    
    func testDivisionWithZero() {
        XCTAssertEqual("00", vc.calcResult(firstArgument: 4, secondArgument: 0, nowOperation: .divide))
        XCTAssertEqual("00", vc.calcResult(firstArgument: 0, secondArgument: 0, nowOperation: .divide))
        XCTAssertEqual("0", vc.calcResult(firstArgument: 0, secondArgument: -3, nowOperation: .divide))
    }
    
    func testDeleteDotAndZero() {
        XCTAssertEqual("12", vc.deleteDotAndZero(value: 12.0))        
        XCTAssertEqual("12.3", vc.deleteDotAndZero(value: 12.3))
    }
}
